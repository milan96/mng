const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost:27017/fruitsDB", {
  useNewUrlParser: true
});

const fruitSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Nisi uneo ime"]
  },
  rating: {
    type: Number,
    max: 10,
    min: 1
  },
  review: String
});

const Fruit = mongoose.model("Fruit", fruitSchema); //ovo je model sa kojim se operise

const fruit = new Fruit({
  name: "Jabuka",
  rating: 9,
  review: "Jabuka je jabuka"
});

// fruit.save();

const personSchema = new mongoose.Schema({
  name: String,
  age: Number,
  favouriteFruit: fruitSchema
});

const Person = mongoose.model("Person", personSchema); //ovo je model sa kojim se operise

const pineapple = new Fruit({
  name: "Pineapple",
  review: "ovo je tekst"
});

pineapple.save();

const person = new Person({
  name: "perica",
  age: 36,
  favouriteFruit: pineapple
});

person.save();

const kivi = new Fruit({
  name: "kivi",
  rating: 4,
  review: "Jabuka asje jabuka"
});

const banana = new Fruit({
  name: "Jabukaasas",
  rating: 9,
  review: "Jabuka je jabuka"
});

// Fruit.insertMany([kivi, banana], function(err) {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log("uspesno");
//   }
// });

Fruit.find(function(err, fruits) {
  if (err) {
    console.log(err);
  } else {
    mongoose.connection.close();

    fruits.forEach(function(fruit) {
      console.log(fruit.name);
    });
  }
});

// Fruit.updateOne(
//   { _id: "5c90a4bcbf8f4576de4f0c40" },
//   { name: "breskva" },
//   function(err) {
//     if (err) {
//       console.log(err);
//     } else {
//       console.log("Uspesno je promenjeno ime");
//     }
//   }
// );

Fruit.deleteOne({ name: "breskva" }, function(err) {
  if (err) {
    console.log(err);
  } else {
    console.log("obrisano");
  }
});
